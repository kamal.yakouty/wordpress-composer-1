module.exports = {
    files: ['assets/css/styles.min.css', 'assets/js/scripts.min.js'],
    injectChanges: true,
    ui: false,
    open: false,
    reloadOnRestart: true,
    proxy: 'wordpress-composer.local.com'
}