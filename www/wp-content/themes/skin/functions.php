<?php

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
		echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php') ) . '</a></p></div>';
	});
	
	add_filter('template_include', function($template) {
		return get_stylesheet_directory() . '/static/no-timber.html';
	});
	
	return;
}

Timber::$dirname = array('templates', 'views');

class StarterSite extends TimberSite {

	function __construct() {
		add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'get_twig', array( $this, 'add_to_twig' ) );
        add_action('vc_before_init', array($this, 'vc_register_shortcodes'));
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', array( $this, 'register_taxonomies' ) );
        $this->register_shortcodes();
		parent::__construct();
	}

	function register_post_types() {
		//this is where you can register custom post types
	}

	function register_taxonomies() {
		//this is where you can register custom taxonomies
	}

    function register_shortcodes( ) {
        require('lib/register_shortcodes.php');
    }
    function vc_register_shortcodes( ) {
        require('lib/vc_register_shortcodes.php');
    }

	function add_to_context( $context ) {
		$context['foo'] = 'bar';
		$context['stuff'] = 'I am a value set in your functions.php file';
		$context['notes'] = 'These values are available everytime you call Timber::get_context();';
		$context['menu'] = new TimberMenu();
		$context['site'] = $this;
		return $context;
	}

	function myfoo( $text ) {
		$text .= ' bar!';
		return $text;
	}

	function add_to_twig( $twig ) {
		/* this is where you can add your own functions to twig */
		$twig->addExtension( new Twig_Extension_StringLoader() );
		$twig->addFilter('myfoo', new Twig_SimpleFilter('myfoo', array($this, 'myfoo')));
		return $twig;
	}

}

new StarterSite();



function my_scripts(){
    wp_enqueue_style( 'main', get_template_directory_uri() . '/assets/css/styles.min.css',false,'1.1','all');
    wp_enqueue_style( 'font-awesome.min', get_template_directory_uri() . '/assets/css/font-awesome.min.css',false,'1.1','all');
    wp_enqueue_script('main-js', get_template_directory_uri() . '/assets/js/scripts.min.js', array('jquery'), '1.0.0', true );
}

add_action('wp_enqueue_scripts', 'my_scripts');

function wpc_show_admin_bar() {
    return false;
}
add_filter('show_admin_bar' , 'wpc_show_admin_bar');


add_action( 'init', 'project_post_type' );
function project_post_type() {
    $labels = array(
        'name' => _x('Réalisations', 'post type general name'),
        'singular_name' => _x('Réalisation', 'post type singular name'),
        'add_new' => _x('Ajouter', 'book'),
        'add_new_item' => __('Ajouter une nouvelle réalisation'),
        'edit_item' => __('Editer la réalisation'),
        'new_item' => __('Nouvelle réalisation'),
        'all_items' => __('Toutes les réalisations'),
        'view_item' => __('Voir réalisation'),
        'search_items' => __('Rechercher réalisations'),
        'not_found' =>  __('Réalisation non trouvée'),
        'not_found_in_trash' => __('Aucune réalisation dans la corbeille'),
        'parent_item_colon' => '',
        'menu_name' => __('Réalisations')

    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array( 'title', 'editor', 'thumbnail')
    );
    register_post_type('project',$args);
    register_taxonomy( 'domaine', 'project', array( 'hierarchical' => false, 'label' => 'Domaine', 'query_var' => true, 'rewrite' => true ) );

}
