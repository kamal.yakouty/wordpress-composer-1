<?php
function banner( $atts, $content = null ) {
    extract(shortcode_atts(array(
        'background_image' => '',
        'text'             => '',
    ), $atts));

    $out = Timber::compile('banner.twig', array(
        'background_image' => new TimberImage($background_image),
        'text' => $text
    ));
    return $out;
}
add_shortcode('banner', 'banner');
