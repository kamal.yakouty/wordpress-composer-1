<?php
vc_map( array(
    'name' => 'Bannière',
    'base' => 'banner',
    'icon' => 'thb_vc_ico_post',
    'class' => 'thb_vc_sc_post',
    'category' => 'Custom',
    'params'    => array(
        array(
            'type' => 'attach_image',
            'heading' => 'Image de fond',
            'param_name' => 'background_image',
            'description' => '',
        ),
        array(
            'type' => 'textarea',
            'heading' => 'Texte affiché',
            'param_name' => 'text',
            'description' => '',
        ),
    ),
    'description' => ''
));
